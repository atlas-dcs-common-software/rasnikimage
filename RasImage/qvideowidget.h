#ifndef QVIDEOWIDGET_H
#define QVIDEOWIDGET_H

#include <QWidget>

class QVideoWidget : public QWidget
{
    Q_OBJECT
public:
    explicit QVideoWidget(QWidget *parent = 0);

    const unsigned char *data;
    QSize pixmap_size;

protected:
    void paintEvent(QPaintEvent *event);

signals:

public slots:

};

#endif // QVIDEOWIDGET_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include "grabber.h"

namespace Ui { class MainWindow; }

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Grabber*       _pGrabber;
    unsigned int   _dServer;
    QString        _sDnsHost;

    void	   cleanUp();
    void           updInfo();
    void           updFrames();
    void	   readAppSettings();
    void	   writeAppSettings();

protected:
    QThread        _pGrabThread;
    unsigned int   _uFrames;

private slots:
    void           onView();
    void           onQuit();
    void           onServer(const QString &sServer);
    void           displayFrame();

signals:

};

#endif // MAINWINDOW_H

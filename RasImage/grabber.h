#ifndef VIDEOGRABBER_H
#define VIDEOGRABBER_H

#include <QObject>

class myImage;	// forward declaration

class Grabber : public QObject
{
    Q_OBJECT
public:
    explicit Grabber(unsigned int frameWidth, unsigned int frameHeight, QObject *parent = 0);
            ~Grabber();

    void    startGrab(const QString& sDnsHost, const unsigned int dServer);
    void    grab();
    void    shouldStop()   { _bStop = true;  }
    void    shouldRun()    { _bStop = false; }
    bool    getRunStatus() { return _bStop;  }

          char* info;
    const char* get_info() const { return info; }

          unsigned char* data;
    const unsigned char* get_data() const { return data; }

    unsigned int   getFrameCounter() const { return _uFrameCounter; }

protected:
    unsigned int   _dServer;
    unsigned int   _uFrameCounter;
    bool           _bStop;
    myImage*       _pDia;

signals:
    void           frameReady();

public slots:

};

#endif // VIDEOGRABBER_H

#include "qvideowidget.h"
#include <QPainter>

static QVector<QRgb> colorTable;

QVideoWidget::QVideoWidget(QWidget *parent) :
    QWidget(parent),
    data(NULL)
{
    setAttribute(Qt::WA_OpaquePaintEvent);

    if (colorTable.empty())
    {
#ifndef MULTICOLOR
        for (int i = 0;   i < 256; i++) colorTable.push_back(qRgb(i, i, i));
#else
        for (int i = 0;   i < 64;  i++) colorTable.push_back(qRgb(i, i*2, 0));
        for (int i = 64;  i < 192; i++) colorTable.push_back(qRgb(i, 127, (i-64)*2));
        for (int i = 192; i < 256; i++) colorTable.push_back(qRgb(i, 2*(i-192)+127, 255));
#endif
    }
}

void QVideoWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    if (!data)
    {
        painter.fillRect(0, 0, width(), height(), Qt::black);
        return;
    }
    QImage img(data, pixmap_size.width(), pixmap_size.height(), QImage::Format_Indexed8);
    img.setColorTable(colorTable);
    painter.drawImage(0, 0, img);
}

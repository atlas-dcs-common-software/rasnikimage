#
# Project file for the RasImage application
#
# To generate a Visual Studio project:
#   qmake -t vcapp RasImage.pro
# To generate a Makefile:
#   qmake RasImage.pro
#
TEMPLATE = app
TARGET   = RasImage

# Create a Qt app
CONFIG += qt thread warn_on exceptions debug_and_release
contains(QT_MAJOR_VERSION,5) {
  QT += core gui widgets concurrent
}

CONFIG(debug, debug|release) {
  OBJECTS_DIR = debug
  MOC_DIR     = debug
  UI_DIR      = debug
  DESTDIR     = ../Debug
  unix: LIBS += -L../Debug
}

CONFIG(release, debug|release) {
  OBJECTS_DIR = release
  MOC_DIR     = release
  UI_DIR      = release
  DESTDIR     = ../Release
  unix: LIBS += -L../Release
}

LIBS += -L../../dim/linux
LIBS += -ldim

INCLUDEPATH += ../include
INCLUDEPATH += ../../dim/dim

FORMS   += RasImage.ui

SOURCES += main.cpp
SOURCES += mainWindow.cpp
SOURCES += grabber.cpp
SOURCES += qvideowidget.cpp

HEADERS += mainWindow.h
HEADERS += grabber.h
HEADERS += qvideowidget.h


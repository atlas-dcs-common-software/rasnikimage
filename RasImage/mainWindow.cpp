#include "mainWindow.h"
#include "interface.h"
#include "ui_RasImage.h"
#include <QDebug>
#include <QSettings>

#define TITLE		"RasImage"
#define VERSION		"1.0"
#define MAX_SERVER	8	// BAL[1-8]

// Setting variables and defaults
const	QString gsOrganization( "Nikhef"   );
const	QString	gsApplication ( "RasImage" );
const	QString	gsVarDnsHost  ( "DnsHost"  );
const	QString	gsRasdimServer( "Server"   );

const	QString gsDfltDnsHost ( "pcatlmdtbal9.cern.ch" );
const	int	giDfltServer  = 1;

static const unsigned int giFrameWidth  = ATLAS_X_IMG;
static const unsigned int giFrameHeight = ATLAS_Y_IMG;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    _pGrabber = new Grabber(giFrameWidth, giFrameHeight);

    ui->setupUi(this);
    QRect r = ui->videoWidget->geometry();
    r.setWidth(giFrameWidth);
    r.setHeight(giFrameHeight);
    ui->videoWidget->setGeometry(r);
    ui->videoWidget->pixmap_size = QSize(giFrameWidth, giFrameHeight);
    ui->videoWidget->data = _pGrabber->get_data();
    ui->labInfo1->setText(_pGrabber->get_info());
    ui->labInfo2->setText("");
    updFrames();

    ui->cbServer->clear();
    for(int i = 1; i <= MAX_SERVER; ++i)
        ui->cbServer->addItem( QString::number(i) );

    connect(ui->butQuit,  SIGNAL(clicked()),                           this, SLOT(onQuit())                );
    connect(ui->butView,  SIGNAL(clicked()),                           this, SLOT(onView())                );
    connect(ui->cbServer, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(onServer(const QString&)));
    connect(_pGrabber,    SIGNAL(frameReady()),                        this, SLOT(displayFrame())          );

    _pGrabber->moveToThread(&_pGrabThread);
    _pGrabThread.start();
    _pGrabber->shouldStop();

    readAppSettings();
    updInfo();
    ui->labDnsHost->setText(_sDnsHost);

    ui->butView->setEnabled(true);
    ui->butQuit->setEnabled(true);
    ui->cbServer->setEnabled(true);
}

MainWindow::~MainWindow()
{
    cleanUp();
}

void MainWindow::onQuit()
{
    writeAppSettings();
    cleanUp();
    exit(0);
}

void MainWindow::cleanUp()
{
    _pGrabber->shouldStop();
    _pGrabThread.quit();
    _pGrabThread.wait();
    delete ui;
}

void MainWindow::onView()
{
    if (!_pGrabThread.isRunning())
    {
        _pGrabThread.start();
    }

    bool    bStop = _pGrabber->getRunStatus();

    ui->butQuit->setDisabled(bStop);
    ui->cbServer->setDisabled(bStop);

    if (bStop == true)
    {
        _uFrames = _pGrabber->getFrameCounter();
        _pGrabber->shouldRun();
        _pGrabber->startGrab(_sDnsHost, _dServer);
        ui->butView->setText("Stop");
        ui->labRunStatus->setText("Running");
    }
    else
    {
        _pGrabber->shouldStop();
        ui->butView->setText("Start");
        ui->labRunStatus->setText("Stopped");
    }
}

void MainWindow::onServer(const QString& sServer)
{
    _dServer = sServer.toInt();
    updInfo();
}

void MainWindow::updInfo()
{
    QString  sInfo;
    sInfo.sprintf("%s%d", RD_SRV_IMAGE, _dServer);
    ui->labServer->setText(sInfo);

    sInfo.sprintf("%s [v%s], Server %d", TITLE, VERSION, _dServer);
    this->setWindowTitle(sInfo);
}

void MainWindow::displayFrame()
{
    QString sInfo = _pGrabber->get_info();

/*
    int     i;
    QChar   qch;
    QString sInfo1, sInfo2;
    int     size  = sInfo.size();

    for(i = 0; i < size; ++i)
    {
	qch = sInfo.at(i);
        if (qch == ':')
            break;
        sInfo1 += qch;
    }
    for(++i ; i < size; ++i)
    {
	qch = sInfo.at(i);
        if (qch != ' ')
            break;
    }
    for( ; i < size; ++i)
    {
        qch = sInfo.at(i);
        sInfo2 += qch;
    }
*/

    ui->videoWidget->update();
    ui->labInfo1->setText(sInfo);
    ui->labInfo2->setText("");
    updFrames();
}

void MainWindow::updFrames()
{
    _uFrames = _pGrabber->getFrameCounter();

    QString  sInfo;
    sInfo.sprintf("%d", _uFrames);
    ui->labFrames->setText(sInfo);
}

void MainWindow::readAppSettings()
{
    QSettings	settings( gsOrganization, gsApplication );

    _sDnsHost = settings.value( gsVarDnsHost,   gsDfltDnsHost ).toString();
    _dServer  = settings.value( gsRasdimServer, giDfltServer  ).toInt();

    QString sServer = QString::number( _dServer );
    int     iIndex  = ui->cbServer->findText( sServer);
    ui->cbServer->setCurrentIndex( iIndex );
}

void MainWindow::writeAppSettings()
{
    QSettings	settings( gsOrganization, gsApplication );

    settings.setValue( gsVarDnsHost,   _sDnsHost );
    settings.setValue( gsRasdimServer, _dServer  );
}

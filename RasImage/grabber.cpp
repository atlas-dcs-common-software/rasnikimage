#include <iostream>
#include "grabber.h"
#include "interface.h"
#include <QDebug>
#include <QThread>
#include <errno.h>
#include <dic.hxx>

class myImage : public DimInfo
{
private:
    Grabber*	_pGrabber;

    void    infoHandler()
    {
	unsigned int i;
	unsigned int iSize = getSize();
	if (iSize < sizeof(class Image))
	    return;
	Image*	pImg  = (Image*)getData();

        // copy image
        iSize = pImg->_Ximg * pImg->_Yimg;
        for(i = 0; i < iSize; ++i)
            _pGrabber->data[i] = pImg->_Image[i];

        // copy info
        for(i = 0; i < MAX_INFO; ++i)
            _pGrabber->info[i] = pImg->_Info[i];
        pImg->_Info[MAX_INFO] = '\0';

        _pGrabber->grab();
    }

public:
    myImage(Grabber* pGrabber, const char* server) : DimInfo(server, -1) { _pGrabber = pGrabber; }
};

/**
 *  Grabber
 */
Grabber::Grabber(unsigned int frameWidth, unsigned int frameHeight, QObject *parent) :
    QObject(parent),
    info(new char[MAX_INFO+1]),
    data(new unsigned char[frameWidth*frameHeight])
{
    _dServer       = 0;
    _uFrameCounter = 0;
    _bStop         = true;
    _pDia          = NULL;

    unsigned int frame_size = frameWidth * frameHeight;
    unsigned int stride     = frame_size >> 8;
    for (unsigned int i = 0; i < 256; ++i)
    {
        memset(data + (i*stride), i, stride);
    }
    memset(data + (255*stride), 255, (frame_size - (255*stride)));
    strncpy(info, "Epoch", MAX_INFO);
}

Grabber::~Grabber()
{
    delete [] info;
    delete [] data;
    if (_pDia)
        delete _pDia;
}

void Grabber::startGrab(const QString& sDnsHost, const unsigned int dServer)
{
    if (dServer == _dServer)
    {
        if (_pDia)
            return;
    }
    _dServer = dServer;

    if (_pDia)
    {
        delete _pDia;
        _pDia = NULL;
    }

    QString     sInfo;
    std::string sUtf8String;

    sUtf8String = sDnsHost.toUtf8().constData();
    DimClient::setDnsNode(sUtf8String.c_str());

    sInfo.sprintf("%s%d", RD_SRV_IMAGE, _dServer);
    sUtf8String = sInfo.toUtf8().constData();
    _pDia = new myImage(this, sUtf8String.c_str());
}

void Grabber::grab()
{
    if (_bStop == false)
    {
        ++_uFrameCounter;
        emit frameReady();
    }
}

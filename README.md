#
# RasImage
#
Qt application to view images created by the Barrel Alignment system of ATLAS.
Runs only on pcatlmdtbal[9,11,12].
In order to compile it, logon the machine vm-tdq-build-01 and
add the following 2 lines to your .bashrc file:
export PATH=/usr/lib64/qt4/bin:${PATH}
export PATH=${PATH}:/sw/atlas/sw/lcg/external/qt/4.8.4-f642c/x86_64-slc6-gcc47-opt/bin


/**
 *  \file   interface.h
 *  \brief  Include file containing the interface between the \e Rasdim server and its clients.
 *
 *  \author Robert.Hart@nikhef.nl
 *  \date   First version: 2004
 */

#ifndef INTERFACE_H
#define INTERFACE_H

// Make sure NO_ERROR is defined and equals 0.
#ifndef NO_ERROR
#define NO_ERROR                0
#endif
#if     NO_ERROR!=0
#error  "NO_ERROR!=0"
#endif

// Rasdim Errors
#define RDE_ILLCMD              1
#define RDE_MUX                 2
#define RDE_GRABBER             3
#define RDE_ANALYSIS            4
#define RDE_OUTPUT              5
#define RDE_WRONGSERVER         6
#define RDE_ILLBKGRND           7
// next error codes are used by clients only
#define RDE_NOSERVER            8       
#define RDE_TIMEOUT             9
#define RDE_INTERNAL            10

// Commands
#define CMD_QUIT                0x0100
#define CMD_PROBE               0x0101
#define CMD_LED_ON              0x0102
#define CMD_LED_OFF             0x0103
#define CMD_VIEW                0x0104
#define CMD_GRAB                0x0105
#define CMD_ANAL                0x0106
#define CMD_MUX_OFF             0x0107

// Auxiliary commands (masks)
#define AUX_SAVE_IMAGE          0x010
#define AUX_DONT_SAVE_BADIMAGE  0x020

// DIM definitions
#define RD_SERVER               "Rasdim"
#define RD_RPC                  "RD_Command"
#define RD_RPC_FORMAT_IN        "I:2;C:32;I:79;F:32"            // format belonging to class Command
#define RD_RPC_FORMAT_OUT       "C:32;I:5;C:20;F:8;I:1;F:16"    // format belonging to class Result
#define RD_SRV_IMAGE            "RD_Image"
#define RD_SRV_IMAGE_FORMAT     "C:128;I:2;C"                   // format belonging to class Image
#define RD_SRV_WINDOUT          "RD_Windout"
#define RD_SRV_WINDOUT_FORMAT   "C"

#define MAX_CHAN_NAME           31
#define MAX_INFO                127
#define MAX_DATE_STR            19
#define MAX_SPOTS               8
#define MAX_WINDOUT             2048

#define MAX_NIN                 32      /**< Maximum number of \e integer parameters for analysis */
#define MAX_FIN                 32      /**< Maximum number of \e float parameters for analysis */

// maximum dimension of images */
#define MAX_X_IMG               768     /**< Maximum horizontal image pixel size */
#define MAX_Y_IMG               576     /**< Maximum vertical image pixel size */

// alignment dimensions for the analysis
#define ATLAS_X_IMG             392     /**< Horizontal image pixel size as used by the barrel alignment of ATLAS */
#define ATLAS_Y_IMG             292     /**< Vertical image pixel size as used by the barrel alignment of ATLAS */

// dimensions of the Rasnik camera's
#define VV5430_WIDTH            384     /**< Horizontal pixel size of the VV5430 CMOS sensor */
#define VV5430_HEIGHT           287     /**< Vertical pixel size of the VV5430 CMOS sensor */

// Analysis types
#define T_RASNIK                0
#define T_SPOT                  1
#define T_FOAM                  2
#define T_SEFO                  4
#define T_SOAP                  8
#define T_DOAP                  16

// Predefined indices for Nin
#define I_NIN_COUNT             0
#define I_FIN_COUNT             1
#define I_VERSION               2
#define I_X_IMG                 3
#define I_Y_IMG                 4

/**
 *  \typedef    BYTE    unsigned char
 *  \brief      The images to be analyzed consist of BYTE's.
 */
typedef unsigned char   BYTE;

/**
 *  \class  MuxAddress
 *  \brief  This class contains the full address of the channel (i.e. camera and led).
 */
class MuxAddress
{
public:
    int         _LedServer;
    int         _LedTopMux;
    int         _LedMasterMux;
    int         _LedRasMux;
    int         _CamServer;
    int         _CamTopMux;
    int         _CamMasterMux;
    int         _CamRasMux;
};

/**
 *  \class  I2c
 *  \brief  This class contains all information about the I2C settings of a channel.
 */
class I2c
{
public:
    int         _rSetup1;
    int         _Normalbacklit;
    int         _Linearmode;
    int         _Autogain;
    int         _Inhibitblack;
    int         _Enableauto;
    int         _Horshuffle;
    int         _Vershuffle;
    int         _Forceblack;
    int         _Div0;
    int         _Div1;

    int         _rSetup2;
    int         _Readmode;
    int         _Pixelmode;
    int         _Pixthreshold;
    int         _Shufflemode;

    int         _rSetup3;
    int         _Pixeloffset;
    int         _Enablesno;
                
    int         _rAnalog;
    int         _Antiblooming;
    int         _Blackreference;
    int         _Whitethreshold;
    int         _Binairisation;

    int         _rGain,   _Gain;
    int         _rCoarse, _Coarse;
    int         _rFine,   _Fine;
    int         _rLower,  _Lower;
    int         _rUpper,  _Upper;
};

/**
 *  \class  Param
 *  \brief  This class contains the analysis arguments of a channel.
 */
class Param
{
public:
    int         _AnalType;
    int         _Nin[MAX_NIN];
    float       _Fin[MAX_FIN];
};

/**
 *  \class  Command
 *  \brief  This class, with only public variables and no methods, represents a \e request as send by
 *          a client and received by a Rasdim server.
 */
class Command
{
public:
    int         _Cmd;
    int         _AuxCmd;
    char        _Channel[MAX_CHAN_NAME+1];
    int         _ChannelId;
    int         _SequenceNr;
    MuxAddress  _MuxAddress;
    int         _Background;
    int         _NImages;
    I2c         _I2c;
    Param       _Param;
};

/**
 *  \class  Result
 *  \brief  This class, with only public variables and no methods, represents a \e reply as returned by
 *          a Rasdim server to a client.
 */
class Result
{
public:
    char        _Channel[MAX_CHAN_NAME+1];
    int         _ChannelId;
    int         _SequenceNr;
    int         _Result;
    int         _AnalResult;
    long        _Time;
    char        _CTime[MAX_DATE_STR+1];
    float       _X;
    float       _Y;
    float       _Scale;
    float       _RotZ;
    float       _ErrX;
    float       _ErrY;
    float       _ErrScale;
    float       _ErrRotZ;
    int         _NSpots;
    float       _XSpot[MAX_SPOTS];
    float       _YSpot[MAX_SPOTS];
};

/**
 *  \class  Image
 *  \brief  This class is used to expose/publish the grabbed image by means of a DimService.
 */
class Image
{
public:
    char        _Info[MAX_INFO+1];
    int         _Ximg;
    int         _Yimg;
    BYTE        _Image[MAX_X_IMG*MAX_Y_IMG];
};

#endif  /*INTERFACE_H*/


